<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function calc(Request $request)
    {
        $operation = $this->checkOperation($request->words);

        $stringNumber = $this->getStringNumber($operation, $request->words);

        $firstIntNumber = $this->stringToInteger($stringNumber[0]);
        $secondIntNumber = $this->stringToInteger($stringNumber[1]);

        $resultInt = $this->doCalc($firstIntNumber, $secondIntNumber, $operation);

        $finalResult = $request->words . ' adalah ' . $this->intToWord($resultInt);

        // dd($firstIntNumber, $secondIntNumber, $request->words . ' adalah ' . $this->intToWord($resultInt));
        return redirect()->route('home')->with('result', $finalResult);
    }

    public function checkOperation($words)
    {
        if (strpos($words, 'tambah') !== false) {
            return 'tambah';
        } else if (strpos($words, 'kurang') !== false) {
            return 'kurang';
        } else if (strpos($words, 'kali') !== false) {
            return 'kali';
        }
    }

    public function getStringNumber($operation, $words)
    {
        $result = explode($operation,$words);

        return $result;
    }

    public function stringToInteger($stringNumber)
    {
        if (strpos($stringNumber, 'belas') !== false) {
            if($this->removeWhiteSpace($stringNumber) == 'sebelas') {
                return 11;
            } else {
                $singleWord = explode('belas', $stringNumber);
                return (int)('1' . $this->wordToInteger($this->removeWhiteSpace($singleWord[0])));
            }
        } else if (strpos($stringNumber, 'puluh') !== false) {
            if($this->removeWhiteSpace($stringNumber) == 'sepuluh') {
                return 10;
            } else {
                $singleWord = explode('puluh', $stringNumber);
                if($singleWord[1] == ' ') {
                    return (int)($this->wordToInteger($this->removeWhiteSpace($singleWord[0])) . '0');
                } else {
                    return (int)($this->wordToInteger($this->removeWhiteSpace($singleWord[0])) . $this->wordToInteger($this->removeWhiteSpace($singleWord[1])));
                }
            }
        } else {
            return $this->wordToInteger($this->removeWhiteSpace($stringNumber));
        }
    }

    public function removeWhiteSpace($string)
    {
        return preg_replace('/\s+/', '', $string);

    }

    public function wordToInteger($word)
    {
        switch ($word) {
            case 'nol' :
                return 0; break;
            case 'satu' :
                return 1; break;
            case 'dua' :
                return 2; break;
            case 'tiga' :
                return 3; break;
            case 'empat' :
                return 4; break;
            case 'lima' :
                return 5; break;
            case 'enam' :
                return 6; break;
            case 'tujuh' :
                return 7; break;
            case 'delapan' :
                return 8; break;
            case 'sembilan' :
                return 9; break;
        }
    }

    public function doCalc($firstIntNumber, $secondIntNumber, $operation)
    {
        if ($operation == 'tambah') {
            return $firstIntNumber + $secondIntNumber;
        } else if ($operation == 'kurang') {
            return $firstIntNumber - $secondIntNumber;
        } if ($operation == 'kali') {
            return $firstIntNumber * $secondIntNumber;
        }
    }

    public function intToWord($number)
    {
        $result = '';
        $satuan = ['puluh', 'ratus', 'ribu'];

        $length = strlen($number);

        if($number < 0) {
            $addFrontWord = 'minus ';
        } else {
            $addFrontWord = '';
        }

        $number = str_split($number);

        for($i = 0; $i<$length; $i++) {
            if ($length > 1 && $number[$i] == 0)
            {
                if ($i == $length)
                {
                    break;
                }

                continue;
            }

            if ($length - $i == 4 && $number[$i] == 1) {
                $result .= 'seribu ';

                continue;
            }

            if ($length - $i == 3 && $number[$i] == 1) {
                $result .= 'seratus ';

                continue;
            }

            if ($length - $i == 2 && $number[$i] == 1) {
                if($number[$i+1] == 0) {
                    $result .= 'sepuluh';
                    break;
                } else if($number[$i+1] == 1) {
                    $result .= 'sebelas';
                    break;
                }

                $result .= $this->intToString($number[$i+1]) . ' belas';
                break;
            }

            $angka = $this->intToString($number[$i]);
            $digit = @$satuan[$length - $i - 2] ?: '';

            $result .= $angka . ' ' . $digit . ' ';
        }

        return $addFrontWord . $result;
    }

    public function intToString($number)
    {
        switch ($number) {
            case 1 :
                return 'satu'; break;
            case 2 :
                return 'dua'; break;
            case 3 :
                return 'tiga'; break;
            case 4 :
                return 'empat'; break;
            case 5 :
                return 'lima'; break;
            case 6 :
                return 'enam'; break;
            case 7 :
                return 'tujuh'; break;
            case 8 :
                return 'delapan'; break;
            case 9 :
                return 'sembilan'; break;
            case 0 :
                return 'nol'; break;
        }
    }
}
