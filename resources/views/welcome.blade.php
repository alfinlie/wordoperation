<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .sub-title {
                font-size: 30px;
                font-weight: 600;
                color: #ff6259;
            }

            .result {
                font-size: 28px;
                font-weight: 400;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title ">
                    Welcome To Text Calculator
                </div>
                <div class="sub-title m-b-md">
                    Please Use Bahasa
                </div>
                <div class="links m-b-md">
                    {{ Form::open(['route' => 'calculation']) }}
                        <input type="text" name="words">
                        <button type="submit">Calculate Now!</button>
                    {{ Form::close() }}
                </div>
                @if(Session::get('result'))
                    <i>Result : {{ Session::get('result') }}</i>
                @endif
            </div>
        </div>
    </body>
</html>
