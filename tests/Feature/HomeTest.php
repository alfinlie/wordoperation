<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCalc()
    {
        $this->visit('/')
            ->type('satu tambah satu', 'words')
            ->press('Calculate Now!')
            ->see('satu tambah satu adalah dua');
    }
}
